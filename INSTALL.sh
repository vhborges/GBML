#!/bin/bash

# =========================================================================
# ---[GBML - Install Script]---
# This file is part of Gaming Backup Multitool for Linux (or GBML for short).
# Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
# See the accompanying COPYING file for more details.
# =========================================================================

# Checking if running as sudo
if [ $UID = 0 ]
then
    # Removing legacy installs
	# - Prior to v1.3.0.0 (before conforming to Linux standards)
    if [ -d /opt/SLSK ]; then
      rm -r /opt/SLSK
    fi
	
	# - Prior to v1.4.0.0 (when it was still called SLSK)
	if [ -d /usr/bin/slsk ]; then
	  rm /usr/bin/slsk /usr/share/applications/steam-linux-swiss-knife.desktop
	  rm -r /usr/share/slsk
    fi
    
    # Creating dirs
    mkdir /usr/share/gbml
    mv ./bin/gbml /usr/bin/gbml
    mv ./bin/SteamLinuxGames.db /usr/share/gbml/SteamLinuxGames.db
    mv ./bin/gaming-backup-multitool-linux.desktop /usr/share/applications/gaming-backup-multitool-linux.desktop
    cp -r ./img /usr/share/gbml/img
    rm -r ./bin
else
    echo "Please run this script as sudo."
    echo "Aborting..."
    echo "--------------------------------------------------------------------------------"
    exit
fi

